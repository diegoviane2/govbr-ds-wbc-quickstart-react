import "@govbr-ds/core/dist/core.min.css";
import "@govbr-ds/webcomponents/dist/webcomponents.umd.min.js";
import { BrowserRouter, Routes, Route, Link } from "react-router-dom";
import Breadcrumb from "components/Breadcrumb/Breadcrumb";
import Cookiebar from "components/Cookiebar/Cookiebar";
import Footer from "components/Footer/Footer";
import Header from "components/Header/Header";
import Menu from "components/Menu/Menu";
import Home from "pages/Home";
import Usuario from "pages/Usuario";


const App = () => {
  return (
    <>
      <header>
        <Header />
      </header>
      <main className="mb-5" id="main">
        <div className="container-lg">
          <div className="row">
            <div className="col-sm-4 col-lg-3">
              <Menu />
            </div>
            <div className="col mb-5">
              <Breadcrumb></Breadcrumb>
              <div className="main-content pl-sm-3 mt-4" id="main-content">
                <BrowserRouter>
                  <Routes>
                    <Route path="/" element={<Home />} />
                    <Route path="/home" element={<Home />} />
                    <Route path="/usuario" element={<Usuario />} />
                  </Routes>
                </BrowserRouter>
              </div>
            </div>
          </div>
        </div>
      </main>
      <Cookiebar />
      <footer>
        <Footer />
      </footer>
    </>
  );
};

export default App;
