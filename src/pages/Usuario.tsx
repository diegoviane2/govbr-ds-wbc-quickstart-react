import { useState } from "react";
import Input from "components/Input/Input";


type data = {
  nome: string | "";
  email: string;
  telefone: string;
  senha: string;
  confirmarSenha: string;
  cpf: string;
  dataNascimento: string;
  endereco: string;
  cep: string;
};

const usuario: data = {
  nome: "",
  email: "",
  telefone: "",
  senha: "",
  confirmarSenha: "",
  cpf: "",
  dataNascimento: "",
  endereco: "",
  cep: "",
};

const error: data = usuario;

const ShowMessage = (props: { success: boolean }) => {
  if (props.success) {
    return (
        <br-message
        id="msg"
        feedback state="success"
        show-icon="true"
      >Formulário enviado com sucesso!</br-message>
    )
  } else {
    return null
  }
}

const Usuario = () => {
  const [values, setValues] = useState<data>(usuario);
  const [errors, setErrors] = useState<data>(error);
  const [success, setSuccess] = useState<boolean>(false);

  const handleChange = (name: string, detail: string) => {
    setValues({ ...values, [name]: detail });
    validateFields(values, name);
    if (Object.keys(errors).length > 0) {
      setErrors(errors);
    } else {
      setErrors(usuario);
    }
  };

  const handleBlur = (name: string, detail: string) => {
    if (Object.keys(errors).length > 0) {
      setErrors(errors);
    } else {
      setErrors(usuario);
    }
  };

  const validateFields = (values: data, field: string) => {
    if (field === 'nome'){
      if (!values.nome) {
        errors.nome = "Nome é obrigatório";
      } else {
        errors.nome = "";
      }
    }

    if (field === 'telefone'){
      if (!values.telefone) {
        errors.telefone = "Telefone é obrigatório";
      } else if (values.telefone.length < 11) {
        errors.telefone = "Telefone deve ter 11 caracteres";
      } else {
        errors.telefone = ""
      }
    }

    if(field === 'email'){
      if (!values.email) {
        errors.email = "Email é obrigatório";
      } else if (!/\S+@\S+\.\S+/.test(values.email)) {
        errors.email = "Email inválido";
      } else {
        errors.email = ""
      }
    }

    if(field === 'senha'){
      if (!values.senha) {
        errors.senha = "Senha é obrigatório";
      } else if (values.senha.length < 6) {
        errors.senha = "Senha deve ter no mínimo 6 caracteres";
      } else {
        errors.senha = ""
      }
    }
    
    if(field === 'confirmarSenha'){
      if (!values.confirmarSenha) {
        errors.confirmarSenha = "Confirmação de senha é obrigatório";
      } else if (values.confirmarSenha !== values.senha) {
        errors.confirmarSenha = "Confirmação de senha deve ser igual a senha";
      } else {
        errors.confirmarSenha = "";
      }
    }

    if(field === 'cpf'){
      if (!values.cpf) {
        errors.cpf = "CPF é obrigatório";
      } else if (values.cpf.length < 11) {
        errors.cpf = "CPF deve ter 11 caracteres";
      } else {
        errors.cpf = "";
      }
    }

    if(field === 'dataNascimento' ){      
      if (!values.dataNascimento) {
        errors.dataNascimento = "Data de nascimento é obrigatório";
      } else {
        errors.dataNascimento = "";
      }
    }
  };

  const validateFieldsForm = (values: data) => {
    const errors: any = {};
    if (!values.nome) {
      errors.nome = "Nome é obrigatório";
    }

    if (!values.telefone) {
      errors.telefone = "Telefone é obrigatório";
    } else if (values.telefone.length < 11) {
      errors.telefone = "Telefone deve ter 11 caracteres";
    }

    if (!values.email) {
      errors.email = "Email é obrigatório";
    } else if (!/\S+@\S+\.\S+/.test(values.email)) {
      errors.email = "Email inválido";
    }
    
    if (!values.senha) {
      errors.senha = "Senha é obrigatório";
    } else if (values.senha.length < 6) {
      errors.senha = "Senha deve ter no mínimo 6 caracteres";
    }

    if (!values.confirmarSenha) {
      errors.confirmarSenha = "Confirmação de senha é obrigatório";
    } else if (values.confirmarSenha !== values.senha) {
      errors.confirmarSenha = "Confirmação de senha deve ser igual a senha";
    }

    if (!values.cpf) {
      errors.cpf = "CPF é obrigatório";
    } else if (values.cpf.length < 11) {
      errors.cpf = "CPF deve ter 11 caracteres";
    }
    
    if (!values.dataNascimento) {
      errors.dataNascimento = "Data de nascimento é obrigatório";
    }
    return errors;
  };

  const handleSubmit = (event: any) => {
    event.preventDefault();

    const errors = validateFieldsForm(values);

    if (Object.keys(errors).length > 0) {
      setErrors(errors);
    } else {
      setErrors(usuario);
      setSuccess(true)
    }
  };

  const renderErrors = (detalheErro: string) => (
    <br-message feedback state="danger" show-icon="true">
      {detalheErro}
    </br-message>
  );

  return (
    <div className="p-3">
      <ShowMessage success={success} />
      <h1>Cadastro de usuário</h1>
      <form onSubmit={handleSubmit}>
        <br-message feedback state="info" show-icon="true">Todos os campos são de preenchimento obrigatório (menos endereço e CEP)</br-message>
        <div className="mb-3">
          <Input
            type="text"
            id="nome"
            placeholder="Nome"
            label="Nome:"
            value={values.nome || ""}
            state={errors.nome ? "danger" : "default"}
            onChange={(detail: any) => handleChange("nome", detail)}
            onBlur={(detail: any) => handleBlur("nome", detail)}
          />
          {errors.nome && renderErrors(errors.nome)}
        </div>
        <div className="mb-3">
          <div className="row">
            <div className="col">
              <Input
                id="email"
                type="text"
                placeholder="Email"
                label="Email:"
                value={values.email || ""}
                state={errors.email ? "danger" : "default"}
                onChange={(detail: any) => handleChange("email", detail)}
                onBlur={(detail: any) => handleBlur("email", detail)}
              />
              {errors.email && renderErrors(errors.email)}
            </div>
            <div className="col">
              <Input
                type="text"
                id="telefone"
                placeholder="Telefone ex: (11) 99999-9999"
                label="Telefone:"
                mask="(##) #####-####"
                value={values.telefone || ""}
                state={errors.telefone ? "danger" : "default"}
                onChange={(detail: any) => handleChange("telefone", detail)}
                onBlur={(detail: any) => handleBlur("telefone", detail)}
              />
              {errors.telefone && renderErrors(errors.telefone)}
            </div>
          </div>
        </div>
        <div className="mb-3">
          <div className="row">
            <div className="col">
              <Input
                type="password"
                id="senha"
                placeholder="Senha"
                label="Senha:"
                ispassword
                value={values.senha || ""}
                state={errors.senha ? "danger" : "default"}
                onChange={(detail: any) => handleChange("senha", detail)}
                onBlur={(detail: any) => handleBlur("senha", detail)}
              />
              {errors.senha && renderErrors(errors.senha)}
            </div>
            <div className="col">
              <Input
                type="password"
                id="confirmarSenha"
                placeholder="Confirmar senha"
                label="Confirmar senha:"
                ispassword
                value={values.confirmarSenha || ""}
                state={errors.confirmarSenha ? "danger" : "default"}
                onBlur={(detail: any) => handleBlur("confirmarSenha", detail)}
                onChange={(detail: any) =>
                  handleChange("confirmarSenha", detail)
                }
              />
              {errors.confirmarSenha && renderErrors(errors.confirmarSenha)}
            </div>
          </div>
        </div>
        <div className="mb-3">
          <div className="row">
            <div className="col">
              <Input
                type="text"
                id="cpf"
                placeholder="Ex: 999.999.999-99"
                label="CPF:"
                mask="###.###.###-##"
                value={values.cpf || ""}
                state={errors.cpf ? "danger" : "default"}
                onBlur={(detail: any) => handleBlur("cpf", detail)}
                onChange={(detail: any) => handleChange("cpf", detail)}
              />
              {errors.cpf && renderErrors(errors.cpf)}
            </div>
            <div className="col">
              <Input
                type="text"
                id="dataNascimento"
                placeholder="Ex: DD/MM/AAAA"
                mask="##/##/####"
                label="Data de nascimento:"
                value={values.dataNascimento || ""}
                state={errors.dataNascimento ? "danger" : "default"}
                onBlur={(detail: any) => handleBlur("dataNascimento", detail)}
                onChange={(detail: any) =>
                  handleChange("dataNascimento", detail)
                }
              />
              {errors.dataNascimento && renderErrors(errors.dataNascimento)}
            </div>
          </div>
        </div>
        <div className="mb-3">
          <div className="row">
            <div className="col">
              <Input
                type="text"
                id="endereco"
                placeholder="Endereço"
                label="Endereço: (Opcional)"
                value={values.endereco || ""}
                state={errors.endereco ? "danger" : "default"}
                onChange={(detail: any) => handleChange("endereco", detail)}
              />
              {errors.endereco && renderErrors(errors.endereco)}
            </div>
            <div className="col">
              <Input
                type="text"
                id="cep"
                placeholder="Ex: 99999-999"
                label="CEP: (Opcional)"
                mask="#####-###"
                value={values.cep || ""}
                state={errors.cep ? "danger" : "default"}
                onChange={(detail: any) => handleChange("cep", detail)}
              />
              {errors.cep && renderErrors(errors.cep)}
            </div>
          </div>
        </div>
        <div className="mb-3">
          <div className="mb-3 d-flex justify-content-lg-end">
              <br-button 
                class="mr-1"
                label="Enviar Formulário" 
                id="enviar" 
                type="primary" 
                onClick={handleSubmit}></br-button>
          </div>
        </div>
      </form>
    </div>
  );
};

export default Usuario;
