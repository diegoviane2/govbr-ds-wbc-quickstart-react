const Header = () => {
  const links = `[
        { name: 'GOVBR-DS', href: 'https://gov.br/ds', title: 'Padrão Digital de Governo', target: '_blank'},
        { name: 'Dataprev', href: 'https://portal.dataprev.gov.br/', title: 'Dataprev', target: '_blank'},
        { name: 'Serpro', href: 'https://www.serpro.gov.br/', title: 'SERPRO', target: '_blank'}
      ]`;

  const title = "Web Components GOVBR-DS -  Quickstart React";
  const subtitle = "Baseado na v1.6.0 do @govbr-ds/webcomponents";

  const image = {
    src: "govbr-logo.png",
    alt: "logo",
    size: "medium"
  };

  return (
    <br-header
      image={image.src}
      alt={image.alt}
      title={title}
      subtitle={subtitle}
      image-size={image.size}
      has-menu
    >
      <div slot="headerMenu">
        <br-button
          role="option"
          circle
          density="small"
          aria-label="Menu"
          icon="bars"
          data-toggle="menu"
          data-target="#main-navigation"
        ></br-button>
      </div>
      <div slot="headerAction">
        <br-header-action
          title-links="Acesso Rápido"
          list-links={links}
        ></br-header-action>
      </div>
    </br-header>
  );
};

export default Header;
