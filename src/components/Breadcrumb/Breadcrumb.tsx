const BreadHome = () => {
  return (<br-breadcrumb></br-breadcrumb>)
}

const BreadUsuario = () => {
  return (
    <br-breadcrumb
      label="Breadcrumb"
      links="[{
        label: 'Página Inicial',
        url: '/',
        home: true
      },{
        label: 'Página Inicial',
        url: '/home'
      },{
        label: 'Usuário',
        active: true
    }]"></br-breadcrumb>
  )
}

const ShowBreadcrumb = () =>  {
  let location = window.location
  const {pathname} = location
  if(pathname.endsWith('usuario')){
    return <BreadUsuario />;
  }
  else {
    return <BreadHome />;
  }
}

const Breadcrumb = () => {
  return (
    <ShowBreadcrumb />
  );
};

export default Breadcrumb;
