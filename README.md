# Web Components GOVBR-DS - Quickstart React

## Descrição

Projeto exemplificar o uso da [biblioteca de Web Components do GOVBR-DS](https://gov.br/ds/webcomponents "Biblioteca de Web Components do GOVBR-DS") em projetos [React](https://reactjs.org/ "React").

[Visualização ao vivo](https://govbr-ds.gitlab.io/dev/wbc/govbr-ds-wbc-quickstart-react/main/).

## Tecnologias

Esse projeto foi desenvolvido usando:

1. [Biblioteca de Web Components do GOVBR-DS](https://gov.br/ds/webcomponents "Biblioteca de Web Components do GOVBR-DS")
1. [React](https://reactjs.org/ "React").

Para saber mais detalhes sobre Web Components sugerimos que consulte o [MDN](https://developer.mozilla.org/pt-BR/docs/Web/Web_Components "Web Components | MDN").

## Dependências

As principais dependências do projeto são:

1. [GOVBR-DS](https://www.gov.br/ds/ "GOVBR-DS")

1. [Web Components](https://gov.br/ds/webcomponents/ "Web Components GOVBR-DS")

1. [Font Awesome](https://fontawesome.com/ "Font Awesome")

1. [Fonte Rawline](https://www.cdnfonts.com/rawline.font/ "Fonte Rawline")

> O fontawesome e a fonte rawline podem ser importadas de um CDN. Consulte a documentação no site do GOVBR-DS para mais detalhes

## Como executar o projeto?

```sh
git clone git@gitlab.com:govbr-ds/dev/wbc/govbr-ds-wbc-quickstart-react.git

npm install

npm start
```

Após isso acesso ao o projeto vai estar disponível no endereço `http://localhost:3000/`.

OBS: Para contribuir com o projeto o clone pode não ser a maneira correta. Por favor consulte nossos guias sobre como contribuir na nossa [wiki](https://gov.br/ds/wiki/ "Wiki").

### Explicando

Para usar os Web Components GOVBR-DS com o React é preciso seguir os seguintes passos:

#### declarations.d.ts

Na pasta _src_ do seu projeto adicione o arquivo `declarations.d.ts` e o código abaixo, importante observar que todos os componentes que foram uzados em seu projeto devem possuir uma referencia neste arquivo:

```typescript
declare namespace JSX {
    interface IntrinsicElements {
        "br-footer": any;
        "br-header": any;
        "br-menu": any;
    }
}
```
O React vai renderizar os webcomponents como um elemento HTML personalizado [1][2]. Com isso alguns eventos precisam ser incluidos diretamento aos componente, por meio da função "addEventListener", com o objetivo de facilitar o trabalho sugerimos o uso do hook, useChangeComponents, que vai recuperar a referencia de DOM (ref) e assim pode ser associado a um componente React.

```typescript
import { useChangeComponents } from "hooks/useChangeComponents"
const inputRef = useChangeComponents(onChange);
  return (
    <br-input
      label={label}
      labelinlined={labelinlined}
      state={state}
      density={density}
      iconSign={iconSign}
      icon={icon}
      ispassword={ispassword}
      isHighlight={isHighlight}
      ref={inputRef}
      mask={mask}
      onChange={onChange}
      onBlur={onBlur}
      {...rest}
    ></br-input>
  );
```


Esse passo registra os Web Components para o React. Registre os componentes necessários de acordo com a documentação da [biblioteca de Web Components do GOVBR-DS](https://gov.br/ds/webcomponents "Biblioteca de Web Components do GOVBR-DS").

Alguns componentes são divididos em subcomponentes, como por exemplo o _Header_ e o _Footer_. Nesses casos é necessário ter um arquivo de _declarations.d.ts_ definindo os elementos para cada componente. Por favor consulte nosso código de exemplo para esses componentes para entender como fazer.

#### App.js

Inclua essas duas importações no arquivo _App.js_.

```javascript
import "@govbr-ds/core/dist/core.min.css";
import "@govbr-ds/webcomponents/dist/webcomponents.common";
```

## Precisa de ajuda?

> Por favor **não** crie issues para fazer perguntas...

Use nossos canais abaixo para obter tirar suas dúvidas:

-   Site do GOVBR-DS [http://gov.br/ds](http://gov.br/ds)

-   Web Components [https://gov.br/ds/webcomponents/](https://gov.br/ds/webcomponents/)

-   Pelo nosso email [govbr-ds@serpro.gov.br](govbr-ds@serpro.gov.br)

-   Usando nosso canal no discord [https://discord.gg/U5GwPfqhUP](https://discord.gg/U5GwPfqhUP)

## Como contribuir?

Antes de abrir um Merge Request tenha em mente algumas informações:

-   Esse é um projeto opensource e contribuições são bem-vindas.
-   Para facilitar a aprovação da sua contribuição, escolha um título curto, simples e explicativo para o MR, e siga os padrões da nossa [wiki](https://gov.br/ds/wiki/ 'Wiki').
-   Quer contribuir com o projeto? Confira o nosso guia [como contribuir](./CONTRIBUTING.md 'Como contribuir?').

### Commits

Nesse projeto usamos um padrão para branches e commits. Por favor observe a documentação na nossa [wiki](https://gov.br/ds/wiki/ "Wiki") para aprender sobre os nossos padrões.

## Créditos

Os Web Components do GOVBR-DS são criados pelo [SERPRO](https://www.serpro.gov.br/ "SERPRO | Serviço Federal de Processamento de Dados") e [Dataprev](https://www.dataprev.gov.br/ "Dataprev | Empresa de Tecnologia e Informações da Previdência") juntamente com a participação da comunidade.

## Licença

Nesse projeto usamos a licença MIT.

## Referência

1 - https://developer.mozilla.org/en-US/docs/Web/API/Web_components/Using_custom_elements
2 - https://react.dev/reference/react-dom/components#custom-html-elements